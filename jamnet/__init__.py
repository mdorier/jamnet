# (C) 2019 The University of Chicago
# See COPYRIGHT in top-level directory.
import six
import tensorflow.keras.models
import _jamnet

def merge(model1, model2, cut_at_offset=None, cut_at_layer=None):
    assert cut_at_offset or cut_at_layer
    assert not (cut_at_offset and cut_at_layer)
    try:
        __models_can_merge(model1, model1)
    except AssertionError as error:
        print(error)
        raise RuntimeError("models have a different architecture and cannot be merged")
    if cut_at_offset:
        return __merge_at_offset(model1, model2, cut_at_offset)
    if isinstance(cut_at_layer, six.string_types):
        return __merge_at_layer_name(model1, model2, cut_at_layer)
    if isinstance(cut_at_layer, int):
        return __merge_at_layer_index(model1, model2, cut_at_layer)

def __merge_at_offset(model1, model2, offset):
    output_model = keras.models.clone_model(model1)
    arrays1 = []
    for l in model1.layers:
        weights = l.get_weights()
        arrays1.extend(weights)
    arrays2 = []
    for l in model2.layers:
        weights = l.get_weights()
        arrays2.extend(weights)
    arrays_out = _jamnet.merge_at_offset(arrays1, arrays2, offset)
    i = 0
    for l in output_model.layers:
        num_arrays_needed = len(l.weights)
        weights = arrays_out[i:(i+num_arrays_needed)]
        i += num_arrays_needed
        l.set_weights(weights)
    return output_model

def __merge_at_layer_name(model1, model2, name):
    output_model = keras.models.clone_model(model1)
    current_model_index = 0
    models = (model1, model2)
    for i in range(0, len(model1.layers)):
        if model1.layers[i].name == name:
            current_model_index = 1
        output_model.layers[i].set_weights(models[current_model_index].layers[i].get_weights())
    return output_model

def __merge_at_layer_index(model1, model2, index):
    if index == 0:
        output_model = keras.models.clone_model(model2)
        output_model.set_weights(model2.get_weights())
        return output_model
    elif index >= len(model1.layers):
        output_model = keras.models.clone_model(model1)
        output_model.set_weights(model1.get_weights())
        return output_model
    else:
        output_model = keras.models.clone_model(model1)
        for i in range(0, len(model1.layers)):
            if i < index:
                output_model.layers[i].set_weights(model1.layers[i].get_weights())
            else:
                output_model.layers[i].set_weights(model2.layers[i].get_weights())
        return output_model

def __models_can_merge(model1, model2):
    assert len(model1.layers) == len(model2.layers)
    for i in range(0, len(model1.layers)):
        l1 = model1.layers[i]
        l2 = model2.layers[i]
        assert len(l1.weights) == len(l2.weights)
        for j in range(0, len(l1.weights)):
            w1 = l1.weights[j]
            w2 = l2.weights[j]
            assert keras.backend.int_shape(w1) == keras.backend.int_shape(w2)
            assert w1.dtype == w2.dtype
