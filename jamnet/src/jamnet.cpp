/*
 * (C) 2019 The University of Chicago
 * 
 * See COPYRIGHT in top-level directory.
 */
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>

namespace py11 = pybind11;
namespace np = py11;

static py11::list merge_at_offset(const std::vector<np::array>& l1, const std::vector<np::array>& l2, ssize_t offset) {
    // check size of lists
    if(l1.size() != l2.size()) {
        throw std::runtime_error("List of arrays must have the same number of elements");
    }
    // check each array individually
    for(unsigned i = 0; i < l1.size(); i++) {
        const auto& array1 = l1[i];
        const auto& array2 = l2[i];
        // compare number of dimensions
        if(array1.ndim() != array2.ndim()) {
            std::stringstream ss;
            ss << "Arrays at index " << i << " of lists do not have the same dimensions";
            throw std::runtime_error(ss.str());
        }
        // compare datatypes
        if(array1.itemsize() != array2.itemsize()) {
            std::stringstream ss;
            ss << "Arrays at index " << i << " do not have the same item type sizes";
            throw std::runtime_error(ss.str());
        }
        // compare shape
        for(unsigned j = 0; j < array1.ndim(); j++) {
            if(array1.shape(j) != array2.shape(j)) {
                std::stringstream ss;
                ss << "Arrays at index " << i << " do not have the same shape";
                throw std::runtime_error(ss.str());
            }
        }
        // make sure arrays are contiguous in memory
        if(!(array1.flags() & (np::array::f_style | np::array::c_style))) {
            throw std::runtime_error("Non-contiguous numpy arrays not yet supported");
        }
        if(!(array2.flags() & (np::array::f_style | np::array::c_style))) {
            throw std::runtime_error("Non-contiguous numpy arrays not yet supported");
        }
    }
    // do the actual merging
    py11::list result;
    for(unsigned i = 0; i < l1.size(); i++) {
        const auto& array1 = l1[i];
        const auto& array2 = l2[i];
        if(array1.nbytes() <= (ssize_t) offset) {
            offset -= array1.nbytes();
            result.append(array1);
        } else if(offset <= 0) {
            result.append(array2);
        } else {
            std::vector<ssize_t> shape(array1.shape(), array1.shape() + array1.ndim());
            std::vector<ssize_t> strides;
            auto tmp = np::array(array1.dtype(), shape, strides);
            std::memcpy((uint8_t*)tmp.data(),        (uint8_t*)array2.data(),        offset);
            std::memcpy((uint8_t*)tmp.data()+offset, (uint8_t*)array1.data()+offset, array1.nbytes()-offset);
            result.append(tmp);
            offset = 0;
        }
    }
    return result;
}

PYBIND11_MODULE(_jamnet, m)
{
    try { py11::module::import("numpy"); }
    catch (...) {
        std::cerr << "[JamNet] Error: could not import numpy at C++ level" << std::endl;
        exit(-1);
    }
    m.def("merge_at_offset", merge_at_offset);
}
