import matplotlib.pyplot as plt
import numpy as np
import copy
from os import listdir

plt.rcParams.update({'font.size': 18})

def read_results_in_csv_file(filename):
    """
    Reads the results from a CSV file and returns a dictionary.
    The dictionary associates to each model name, a pair [ loss, accuracy ].
    """
    data = dict()
    with open(filename) as f:
        for line in f:
            if line.startswith('#'):
                continue
            d = line.split(',')
            data[d[0]] = [ float(d[1]), float(d[2]) ]
    return data
            
def get_column(data, col):
    """
    Get a particular column from the CSV file loaded in the data dictionary.
    """
    x = []
    if col == 0:
        return sorted(data)
    else:
        for k in sorted(data):
            x.append(data[k][col-1])
    return x

def plot_original_loss(original_loss, imagefile):
    """
    Plots the loss function of the original network.
    """
    fig, ax = plt.subplots()
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, bottom=.18, left=.18)
    ax.plot(original_loss, color='b', linewidth=0.7, label='original')
    ax.set(xlabel='batches', ylabel='loss')
    ax.grid()
    ax.legend()
    fig.savefig(imagefile)
    plt.close()

def plot_original_relative_loss(original_loss, imagefile):
    """
    Plots the loss variations of the original network.
    """
    fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, gridspec_kw={'width_ratios':[3, 1]})
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, bottom=.18, left=.18)
    loss = []
    for i in range(0, len(original_loss)-1):
        loss.append((original_loss[i+1] - original_loss[i])/original_loss[i])
    ax1.plot(loss, color='b', linewidth=0.7, label='original')
    ax1.set(xlabel='timeline (batches)', ylabel='relative loss')
    ax1.grid()
    ax2.hist(loss, color='c', histtype='stepfilled', bins=30, orientation='horizontal')
    ax2.set(xlabel='histogram')
    ax2.grid()
    fig.savefig(imagefile)
    plt.close()

def plot_merged_loss(original_loss, merged_files, imagefile):
    """
    Plots the loss of merged models.
    """
    fig, ax = plt.subplots()
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, bottom=.18, left=.18)
    max_loss = None
    min_loss = None
    for f in merged_files:
        data = read_results_in_csv_file(f)
        loss = get_column(data, 1)
        if max_loss is None:
            max_loss = copy.deepcopy(loss)
        if min_loss is None:
            min_loss = copy.deepcopy(loss)
        for i in range(0, len(loss)):
            if max_loss[i] < loss[i]:
                max_loss[i] = loss[i]
            if min_loss[i] > loss[i]:
                min_loss[i] = loss[i]
    ax.plot(max_loss, color='g', linewidth=0.7, label='max merged')
    ax.plot(min_loss, color='r', linewidth=0.7, label='min merged')
    ax.plot(original_loss, color='b', linewidth=0.7, label='original')
    ax.set(xlabel='batches', ylabel='loss')
    ax.legend()
    ax.grid()
    fig.savefig(imagefile)
    plt.close()

def plot_merged_loss_relative(original_loss, merged_files, imagefile):
    """
    Plots the loss of the merged models relative to that of the original model.
    """
    fig, ax = plt.subplots()
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, bottom=.18, left=.18)
    x = []
    y = []
    for f in merged_files:
        data = read_results_in_csv_file(f)
        loss = get_column(data, 1)
        for i in range(0, len(loss)):
            orig = sorted([original_loss[i], original_loss[i+1]])
            if(loss[i] > orig[1]):
                loss[i] = (loss[i] - orig[1])/orig[1]
            elif(loss[i] < orig[0]):
                loss[i] = (loss[i] - orig[0])/orig[0]
            else:
                loss[i] = 0
            x.append(i)
            y.append(loss[i])
    ax.scatter(x, y, s=2, color='orange')
    ax.set(xlabel='batches', ylabel='relative loss')
    ax.grid()
    fig.savefig(imagefile)
    plt.close()

def plot_retrained_loss(original_loss, retrained_files, imagefile):
    """
    Plots the loss of merged models after retraining
    """
    fig, ax = plt.subplots()
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, bottom=.18, left=.18)
    max_loss = None
    min_loss = None
    for f in retrained_files:
        data = read_results_in_csv_file(f)
        loss = get_column(data, 1)
        if max_loss is None:
            max_loss = copy.deepcopy(loss)
        if min_loss is None:
            min_loss = copy.deepcopy(loss)
        for i in range(0, len(loss)):
            if max_loss[i] < loss[i]:
                max_loss[i] = loss[i]
            if min_loss[i] > loss[i]:
                min_loss[i] = loss[i]
    ax.plot(range(31,470), max_loss[0:(470-31)], color='g', linewidth=0.7, label='max retrained')
    ax.plot(range(31,470), min_loss[0:(470-31)], color='r', linewidth=0.7, label='min retrained')
    ax.plot(original_loss, color='b', linewidth=0.7, label='original')
    ax.vlines(30, 0, 2.25, colors='grey', linestyles=':')
    ax.set(xlabel='batches', ylabel='loss')
    ax.legend()
    ax.grid()
    fig.savefig(imagefile)
    plt.close()

def plot_retrained_loss_relative(original_loss, retrained_files, imagefile):
    """
    Plots the relative loss of the merged models after retraining,
    relative to that of the original model.
    """
    fig, ax = plt.subplots()
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, bottom=.18, left=.18)
    x = []
    y = []
    for f in retrained_files:
        data = read_results_in_csv_file(f)
        loss = get_column(data, 1)[0:(470-32)]
        for i in range(0,len(loss)):
            orig = original_loss[i+31]
            x.append(i+31)
            y.append((loss[i]-orig)/orig)
    ax.scatter(x, y, s=2, color='orange')
    ax.set(xlabel='batches', ylabel='relative loss')
    ax.grid()
    fig.savefig(imagefile)
    plt.close()

if __name__ == '__main__':
    original_data = read_results_in_csv_file('originals.csv')
    original_loss = get_column(original_data, 1)
    plot_original_loss(original_loss, 'original.eps')
    plot_original_relative_loss(original_loss, 'original-rel.eps')
    merged_files = [ 'merged-{i:02d}.csv'.format(i=i) for i in range(0, 10) ]
    plot_merged_loss(original_loss, merged_files, 'merged.eps')
    plot_merged_loss_relative(original_loss, merged_files, 'merged-rel.eps')
    retrained_files = [ 'merge-and-retrain-{i:02d}.csv'.format(i=i) for i in range(0, 20) ]
    plot_retrained_loss(original_loss, retrained_files, 'retrained.eps')
    plot_retrained_loss_relative(original_loss, retrained_files, 'retrained-rel.eps')
