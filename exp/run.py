from __future__ import print_function
import sys
import os
import shutil
import random
import tensorflow.keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.models import load_model
from tensorflow.keras import backend as K
import jamnet

def __list_model_names(path, prefix=''):
    """
    This function takes a directory path and an optional prefix,
    and lists all the HDF5 files (.h5 extension) in the provided
    directory that have the provided prefix.
    """
    files = os.listdir(path)
    l = [ path+'/'+f for f in files if f.startswith(prefix) and f.endswith('.h5') ]
    return sorted(l)

class ModelCheckpointEveryBatch(tensorflow.keras.callbacks.Callback):
    """
    This class is used as callback by Keras during learning.
    It checkpoints the model using HDF5, but does so at every
    batch instead of every epoch, contrary to Keras' ModelCheckpoint class.
    """

    def __init__(self, filename):
        """
        Constructor. The filename can be a pattern including a step field,
        for example: test-{step:05d}.h5
        """
        self.__filename = filename
        self.__batches = 0
        self.model = None
        directory = '/'.join(filename.split('/')[0:-1])
        if not os.path.exists(directory):
            os.makedirs(directory)

    def on_batch_end(self, batch, logs={}):
        """
        When a batch ends, we save the model.
        """
        self.__batches += 1
        filename = self.__filename.format(batch=self.__batches)
        self.model.save(filename)

class ModelMerger():
    """
    The class defines a policy for merging Keras models and is callable.
    """

    def __init__(self, cut_at_max_offset=None, cut_at_layer=None):
        """
        Constructor.
        cut_at_max_offset: integer indicating the maximum offset at which a cut can be done.
        cut_at_layer: string layer name at which to cut.
        Use either cut_at_max_offset or cut_at_layer but not both.
        """
        self.__cut_at_max_offset = cut_at_max_offset
        self.__cut_at_layer  = cut_at_layer

    def __call__(self, model1, model2, output_file):
        """
        Parenthesis operator. Takes two Keras models and merges them
        according to the requested policy. The output is written into
        output_file (string).
        """
        if self.__cut_at_max_offset is not None:
            offset = random.randint(0, self.__cut_at_max_offset)
            model = jamnet.merge(model1, model2, cut_at_offset=offset)
        else:
            model = jamnet.merge(model1, model2, cut_at_layer=self.__cut_at_layer)
        model.save(output_file)

def create_model(input_shape, num_classes):
    """
    This function is used to create the Keras model.
    input_shape should be a tuple representing the shape of the input.
    num_classes should be an int representing the number of possible
    classes output by the model.
    In the context of the MNIST dataset, input_shape should be
    (1, 28, 28) (or (28, 28, 1), depending on whether the channel is first
    or last) and num_classes should be 10.
    """
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                            activation='relu',
                            input_shape=input_shape,
                            name='conv2d_1'))
    model.add(Conv2D(64, (3, 3), activation='relu', name='conv2d_2'))
    model.add(MaxPooling2D(pool_size=(2, 2), name='maxpooling2d_1'))
    model.add(Dropout(0.25, name='dropout_1'))
    model.add(Flatten(name='flatten_1'))
    model.add(Dense(128, activation='relu', name='dense_1'))
    model.add(Dropout(0.5, name='dropout_2'))
    model.add(Dense(num_classes, activation='softmax', name='dense_2'))
    return model

def reload_model(weights_from, optimizer_from):
    model = load_model(optimizer_from)
    model.load_weights(weights_from)
    return model

def load_mnist_dataset():
    """
    This function loads the desired dataset from Keras. It returns
    a dataset dictionary with keys x_train, y_train, x_test, y_test,
    input_shape, and num_classes.
    """
    img_rows, img_cols = 28, 28
    num_classes = 10
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    y_train = tensorflow.keras.utils.to_categorical(y_train, num_classes)
    y_test = tensorflow.keras.utils.to_categorical(y_test, num_classes)
    return { 'x_train' : x_train,
             'y_train' : y_train,
             'x_test'  : x_test,
             'y_test'  : y_test,
             'input_shape' : input_shape,
             'num_classes' : num_classes }

def train_model(model, dataset, batch_size, epochs, output_name=None, verbose=1):
    """
    This function trains the provided model on the given dataset with
    a given batch size and number of epochs. If output_name is provided,
    a ModelCheckpointEveryBatch object will be used as callback to
    checkpoint the model at every batch.
    """
    model.compile(loss=tensorflow.keras.losses.categorical_crossentropy,
            optimizer=tensorflow.keras.optimizers.Adadelta(learning_rate=0.1),
            metrics=['accuracy'])
    callbacks = []
    if(output_name is not None):
        callbacks.append(ModelCheckpointEveryBatch(output_name))
    model.fit(dataset['x_train'], dataset['y_train'],
            batch_size=batch_size,
            epochs=epochs,
            verbose=verbose,
            callbacks=callbacks)

def evaluate_models(path, dataset, prefix='', verbose=0):
    """
    This function loads a list of models at a given path and evaluates them
    with the provided dataset. The dataset should be a dictionary containing
    at list the following entries: x_test, y_test, input_shape, num_classes.
    The function will return a dictionary mapping the model's name to
    a score composed of [ loss, accuracy ].
    """
    model_files = __list_model_names(path, prefix)
    result = dict()
    for f in model_files:
        model = load_model(f)
        model.compile(loss=tensorflow.keras.losses.categorical_crossentropy,
            optimizer=tensorflow.keras.optimizers.Adadelta(),
            metrics=['accuracy'])
        score = model.evaluate(dataset['x_test'], dataset['y_test'], verbose=verbose)
        result[f] = score
        print("model "+f+" score is "+str(score))
        K.clear_session()
    return result

def create_merged_models(input_dir, output_dir, merger, input_prefix='', output_prefix='', input_file_index=None):
    """
    This function loads a list of models at a given path and merges
    consecutive models, producing the output in the output_dir directory.
    """
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    input_model_files = __list_model_names(input_dir, input_prefix)
    filenames = []
    if input_file_index is None:
        model2 = load_model(input_model_files[0])
        model1 = None
        f_old = input_model_files[0]
        for f in input_model_files[1:]:
            model1 = model2
            model2 = load_model(f)
            filename = f_old.split('/')[-1]
            if input_prefix == '':
                output_file = output_dir+'/'+output_prefix+filename
            else:
                output_file = output_dir+'/'+filename.replace(input_prefix, output_prefix)
            merger(model1, model2, output_file=output_file)
            filenames.append(output_file)
            f_old = f
    else:
        model1 = load_model(input_model_files[input_file_index])
        model2 = load_model(input_model_files[input_file_index+1])
        filename = input_model_files[input_file_index].split('/')[-1]
        if input_prefix == '':
            output_file = output_dir+'/'+output_prefix+filename
        else:
            output_file = output_dir+'/'+filename.replace(input_prefix, output_prefix)
        merger(model1, model2, output_file=output_file)
        filenames = output_file
    return filenames

def prune_files(output_dir, input_file_index):
    filenames = __list_model_names(output_dir)
    filenames = sorted(filenames)
    for i in range(0, len(filenames)-input_file_index):
        j = len(filenames) - i - input_file_index - 1
        os.rename(filenames[j], filenames[j+input_file_index])

def generate_results_csv(filename, scores):
    """
    This function takes the results and produces a CSV file.
    The scores variable should be a dictionary mapping the model name
    with an array [loss, accuracy].
    """
    with open(filename, 'w+') as f:
        f.write('# model_name,loss,accuracy\n')
        for k in sorted(scores):
            f.write(k+','+','.join([str(x) for x in scores[k]])+'\n')

def run_experiments(output_dir):
    """
    This function runs the experiments for the paper, producing CV files
    with the data that can be plot.
    """
    random.seed(1234)
    print('===> Loading MNIST dataset')
    dataset = load_mnist_dataset()
    print('===> Creating Keras model')
    model = create_model(dataset['input_shape'], dataset['num_classes'])
    print('===> Training the original model')
    output_name = output_dir + '/originals/original-{batch:05d}.h5'
    train_model(model, dataset, batch_size=128, epochs=1, output_name=output_name)
    print('===> Evaluating the original model')
    scores = evaluate_models(output_dir+'/originals', dataset, 'original-')
    print('===> Creating originals.csv file with results')
    generate_results_csv(output_dir+'/originals.csv', scores)
    print('===> Computing maximum offset in model')
    max_offset = 0
    for l in model.layers:
        for w in l.get_weights():
            max_offset += w.nbytes
    print('===> Maximum offset is '+str(max_offset))
    print('===> Starting merge experiment, merging at random offsets')
    for i in range(0, 10):
        output_path = output_dir+'/merged-{i:02d}'.format(i=i)
        output_prefix = 'merged-'
        merger = ModelMerger(cut_at_max_offset=max_offset)
        print('===> Generating merged models '+str(i)+', cutting at random offsets')
        create_merged_models(output_dir+'/originals', output_path, merger, input_prefix='original-', output_prefix=output_prefix)
        print('===> Evaluating merged models from directory '+output_path)
        scores = evaluate_models(output_path, dataset, output_prefix)
        csv_filename = output_dir+'/merged-{i:02d}.csv'.format(i=i)
        print('===> Generating '+csv_filename+' file with results')
        generate_results_csv(csv_filename, scores)
        print('===> Deleting directory with merged models')
        shutil.rmtree(output_path)
        print('===> Done with merge experiment '+str(i))
    print('===> Done with merge experiments')
    print('===> Starting experiment merging and re-training')
    for i in range(0, 20):
        output_path = output_dir+'/merge-and-retrain-{i:02d}'.format(i=i)
        output_prefix = 'merged'
        input_file_index = 30
        merger = ModelMerger(cut_at_max_offset=max_offset)
        print('===> Generating merged models '+str(i)+', cutting at random offsets')
        output_filename = create_merged_models(output_dir+'/originals', 
                output_path, merger, input_prefix='original-',
                output_prefix=output_prefix+'-init-',
                input_file_index=input_file_index)
        print('===> Reloading merged model for further training')
        model = reload_model(weights_from=output_filename, optimizer_from='originals/original-{x:05d}.h5'.format(x=input_file_index))
        print('===> Continuing training from merged model')
        train_model(model, dataset, batch_size=128, epochs=1, output_name=output_path+'/'+output_prefix+'-{batch:05d}.h5')
        print('===> Erasing initial merged model')
        os.remove(output_filename)
        print('===> Prunning files')
        # Files generated will have suffixes starting at 0, we want them to start at input_file_index
        prune_files(output_path, input_file_index)
        print('===> Evaluating trained models originating from the merged model')
        scores = evaluate_models(output_path, dataset, output_prefix)
        csv_filename = output_dir+'/merge-and-retrain-{i:02d}.csv'.format(i=i)
        print('===> Creating '+csv_filename+' file with results')
        generate_results_csv(csv_filename, scores)
        print('===> Deleting directory with merged models')
        shutil.rmtree(output_path)
        print('===> Done with merge experiment (continuing trainig) '+str(i))
    print('===> Erasing original model files')
    shutil.rmtree(output_dir+'/originals')
    print('===> All done')

if __name__ == '__main__':
    if(len(sys.argv) != 2):
        print("Usage: python run.py <outputdir>")
    output_dir = sys.argv[1]
    run_experiments(output_dir)
