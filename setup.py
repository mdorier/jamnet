from distutils.core import setup
from distutils.extension import Extension
from distutils.sysconfig import get_config_vars
import os
import os.path
import sys
import pybind11

(opt,) = get_config_vars('OPT')
os.environ['OPT'] = " ".join(
		    flag for flag in opt.split() if flag != '-Wstrict-prototypes'
		)

files = ["jamnet/src/jamnet.cpp"]

jamnet_module = Extension('_jamnet', files,
        extra_compile_args=['-std=c++11', '-g'],
        include_dirs=[ pybind11.get_include(user=True) ],
        depends=[])

setup(name='jamnet',
      version='0.1',
      author='Matthieu Dorier',
      description="""Python library that merges Keras models""",      
      ext_modules=[ jamnet_module ],
      packages=['jamnet']
     )
