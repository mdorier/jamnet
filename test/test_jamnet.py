from __future__ import print_function
import sys
sys.path.append('../build/lib.linux-x86_64-2.7')
import pytest
import keras
import numpy as np
import theano.ifelse
#from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import jamnet

def __create_model(input_shape, num_classes):
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=input_shape,
                     name='conv2d_1'))
    model.add(Conv2D(64, (3, 3), activation='relu', name='conv2d_2'))
    model.add(MaxPooling2D(pool_size=(2, 2), name='maxpooling_1'))
    model.add(Dropout(0.25, name='dropout_1'))
    model.add(Flatten(name='flatten_1'))
    model.add(Dense(128, activation='relu', name='dense_1'))
    model.add(Dropout(0.5, name='dropout_2'))
    model.add(Dense(num_classes, activation='softmax', name='dense_2'))
    return model

def test_models_can_merge():
    model1 = __create_model((28,28,1), 10)
    model2 = __create_model((28,28,1), 10)
    jamnet.__models_can_merge(model1, model2)

def test_models_cannot_merge():
    model1 = __create_model((28,28,1), 10)
    model2 = __create_model((32,38,1), 8)
    with pytest.raises(AssertionError) as e_info:
        jamnet.__models_can_merge(model1, model2)
    model2 = __create_model((28,28,1), 10)
    model2.add(Dense(6, activation='relu', name='dense_3'))
    with pytest.raises(AssertionError) as e_info:
        jamnet.__models_can_merge(model1, model2)

def test_merge_models_by_layer_name():
    model1 = __create_model((28,28,1), 10)
    model2 = __create_model((28,28,1), 10)
    model3 = jamnet.merge(model1, model2, cut_at_layer='maxpooling_1')
    jamnet.__models_can_merge(model1, model3)
    active_model = model1
    for i in range(0, len(model3.layers)):
        if(model3.layers[i].name == 'maxpooling_1'):
            active_model = model2
        l1 = active_model.layers[i]
        l2 = model3.layers[i]
        w1 = l1.get_weights()
        w2 = l2.get_weights()
        for a1,a2 in zip(w1,w2):
            assert np.array_equal(a1,a2)

def test_merge_models_by_layer_index():
    model1 = __create_model((28,28,1), 10)
    model2 = __create_model((28,28,1), 10)
    model3 = jamnet.merge(model1, model2, cut_at_layer=4)
    jamnet.__models_can_merge(model1, model3)
    active_model = model1
    for i in range(0, len(model3.layers)):
        if(i == 4):
            active_model = model2
        l1 = active_model.layers[i]
        l2 = model3.layers[i]
        w1 = l1.get_weights()
        w2 = l2.get_weights()
        for a1,a2 in zip(w1,w2):
            assert np.array_equal(a1,a2)

def test_merge_models_by_offset():
    model1 = __create_model((28,28,1), 10)
    model2 = __create_model((28,28,1), 10)
    model3 = jamnet.merge(model1, model2, cut_at_offset=-3)
    model4 = jamnet.merge(model1, model2, cut_at_offset=100000000)
    model5 = jamnet.merge(model1, model2, cut_at_offset=1024)
    model6 = jamnet.merge(model1, model2, cut_at_offset=1025)
    model7 = jamnet.merge(model1, model2, cut_at_offset=1026)
